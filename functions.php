<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

try {
    $pdo = new PDO('mysql:host=localhost;dbname=phonebook;charset=utf8', 'root', ''); 
} catch (PDOException $e) {
    echo 'Подключение не удалось: ' . $e->getMessage();
}


function getContacts($pdo) {
    $contacts = [];
    $sql = 'SELECT  id, firstname, middlename, lastname, phones '
          . 'FROM contact ORDER BY firstname';
    $query = $pdo->query($sql);
    foreach ($query as $row) {
		$contacts[] = $row;
	}
	return $contacts;
}


function getContact($pdo, $id) {
    $sql = 'SELECT  id, firstname, middlename, lastname, phones '
          . 'FROM contact '
          . 'WHERE id=' . $id;
    $query = $pdo->query($sql) ;
    return $query->fetch();
}


function addContact($pdo, $contact) {
    $sql = "INSERT INTO `phonebook`.`contact` (`firstName`, `middleName`, `lastName`, `phones`) "
			. "VALUES ('$contact[firstname]', '$contact[middlename]', '$contact[lastname]', '$contact[phones]')";
    $pdo->exec($sql) ;
}


function delContact($pdo, $id) {
    $sql = 'DELETE FROM contact '
          . 'WHERE id=' . $id;
    $pdo->query($sql) ;
}

