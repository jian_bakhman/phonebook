-- phpMyAdmin SQL Dump
-- version 4.4.11
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Время создания: Фев 13 2016 г., 01:57
-- Версия сервера: 5.5.44-log
-- Версия PHP: 5.4.41

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `phonebook`
--
CREATE DATABASE IF NOT EXISTS `phonebook` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `phonebook`;

-- --------------------------------------------------------

--
-- Структура таблицы `contact`
--

CREATE TABLE IF NOT EXISTS `contact` (
  `id` smallint(5) unsigned NOT NULL,
  `firstName` varchar(15) NOT NULL,
  `lastName` varchar(20) DEFAULT NULL,
  `middleName` varchar(20) DEFAULT NULL,
  `phones` varchar(200) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `contact`
--

INSERT INTO `contact` (`id`, `firstName`, `lastName`, `middleName`, `phones`) VALUES
(1, 'Иван', 'Иванов', 'Иванович', '["+38-050-900-00-01","+35-85-65-95-86"]'),
(2, 'Сергей', 'Сегреев', 'Сергеевич', '["+38-050-900-00-00","+38-050-900-00-03","+38-050-900-00-04"]'),
(3, 'Петр', 'Петров', 'Петрович', '["+38-050-900-00-02","+3-3-58-65-25-98"]'),
(4, 'Александр', 'Александров', 'Александрович', '[]'),
(6, 'Василий', 'Васильевич', 'Васин', '["+50"]'),
(7, 'Семен', 'Семенович', 'Семенов', '[]'),
(9, 'Федор', 'Федоров', 'Федорович', '["2983479347"]');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `contact`
--
ALTER TABLE `contact`
  MODIFY `id` smallint(5) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
