<?php
/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once 'functions.php';

$phone = [];
$phones = [];
if (isset($_GET['id'])) {
	$id = $_GET['id'];
}

if (isset($_POST[firstname]) && $_POST[firstname] != '') {
    for ($i = 0; $i < 10; $i++){
        if (!empty($_POST[$i])) $phones[] = $_POST[$i];
    }
    $phones = json_encode($phones);
	$sql = "UPDATE contact "
		. "SET firstname = '$_POST[firstname]', "
		. "middlename = '$_POST[middlename]' , "
		. "lastname = '$_POST[lastname]' , "
		. "phones = '$phones' "
		. "WHERE id = '$id'";
	$pdo->exec($sql);
	header('location: index.php');
} else {
    if (isset($_POST['firstname'])) $noname = TRUE;
	$contact = getContact($pdo, $id);
    $phones = json_decode($contact['phones'], $assoc = FALSE);
    foreach ($phones as $value) {
		$phone[] = $value;
	}?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Телефонная книга</title>
</head>
<body>
	<h1>Телефонная книга</h1>

    <form method="POST">

        <div>
            <label>Имя</label>
            <input type="text" name="firstname" value="<?php echo $contact[firstname]; ?>"/>
                <?php echo $noname ? 'Поле не может быть пустым' : ''; ?>
        </div>

        <div>
            <label>Отчество</label>
            <input type="text" name="middlename" value="<?php echo $contact[middlename]; ?>"/>
        </div>

        <div>
            <label>Фамилия</label>
            <input type="text" name="lastname" value="<?php echo $contact[lastname]; ?>"/>
        </div>
		<br>
		
		<div>
            <label>Телефоны:</label><br>
			<?php for ($i = 0; $i < 10; $i++){ ?>
            <input type="text" name="<?php echo $i; ?>" value="<?php echo $phone[$i]; ?>"/><br>
			<?php } ?>
        </div>

        <div>
            <input type="submit" value="Сохранить" />
        </div>

    </form>
  
</body>
</html>

<?php }        